
//export const SHARE_URI_BASE = '/zth2Api';


const URI_BASE = 'https://api.zetianhui.net/zth2Api';

export const URI_INDEXDATA = URI_BASE + '/smallGood/index';

export const SHARE_URI_BASE = 'https://www.zetianhui.xyz/zth2Api';
export const URI_LOGIN = SHARE_URI_BASE + '/zthUser/userLogin';

//（6）接口名称：获取小编推荐指定模块的商品列表
export const URI_RECOMMENDDETAILS = SHARE_URI_BASE + '/recommend/goods';
export const GOODSDETAILS = SHARE_URI_BASE + '/good/goodDetails';
export const GOODYOULIKE = SHARE_URI_BASE + '/good/goodYouLike';
export const GETTKL = SHARE_URI_BASE + '/coupon/tkl';
export const GETBRANDLIST = SHARE_URI_BASE + '/good/queryBrandId';

//（80）接口名称:查询活动商品
export const URI_GETGOODSACTIVITY = SHARE_URI_BASE + '/good/goodsActivity';

export const GETGOODSDETAILS = SHARE_URI_BASE + '/good/goodIdDetail';

//wx分享
export const GETWXCONFIG = SHARE_URI_BASE + '/good/share';

//***************************

//(3)接口名称：获取底部导航
export const GETTABBAR = SHARE_URI_BASE + '/index/bottomNavigation2';
//（5）接口名称：获取小编推荐列表
export const GETRECOMENDLIST = SHARE_URI_BASE + '/recommend/list';
//（10）接口名称：查询商品分类
export const GETTOPGOODCATE = SHARE_URI_BASE + '/good/queryGoodCate';

//(7) 接口名称：获取首页分类制定模块的商品列表
export const URI_GETLAYOUTTYPELIST = SHARE_URI_BASE + '/good/queryByType';

export const GETUSERINFO = SHARE_URI_BASE + '/zthUser/getUser';

//(11)接口名称：根据商品分类ID 查询商品列表
export const FENLEILIST = SHARE_URI_BASE + "/good/queryByGoodCateId";

export const GETBANNER = SHARE_URI_BASE + '/index/banners';

//good/queryNewAllSearch  （53）接口名称：新版搜索接口
export const GETSEARCHRESULT = SHARE_URI_BASE + '/good/queryNewAllSearch';

/**
 * 序号: 43
 * 名称：搜券关键字
 * 请求：GET
 * 说明：获取搜券关键字数据
 */

export const GETHOTSKEY = SHARE_URI_BASE + '/good/queryHotSearch';

export const URI_GETMODULE = SHARE_URI_BASE + '/index/iconsConfig';

//
export const URI_INDEXCOLUMNICONCONFIG = SHARE_URI_BASE + '/index/columnIconConfig';

export const URI_GETUSERINFO =  SHARE_URI_BASE + '/my/security/userInfo';

//(20)接口名称:我的页面查询提现金额 1、2粉丝收益，累计提现
export const GETTIXIANMONEY = SHARE_URI_BASE + '/my/security/profit';

//(21)接口名称：我的页面8大模块下发数据

export const URI_PAGECONFIG = SHARE_URI_BASE + '/my/pageConfig';

//（64）接口名称：新版品牌 推荐品牌------> 品牌精选页面
export const URI_PAGE_BRAND = SHARE_URI_BASE + '/good/recommendGoodBrand';


//(87)获取答题红包
export const URI_GET_ANSWER_HONGBAO = SHARE_URI_BASE + '/activity/security/getAnswerHongBao';

//发送手机验证码

export const GET_Y_Z_CODE = SHARE_URI_BASE + '/verifyCode/sendVerifyCode';

//(88)接口名称：登录或注册  分享出来的需要填邀请码

export const URI_WEB_LOGIN = SHARE_URI_BASE + '/zthUser/userLoginInPage';



